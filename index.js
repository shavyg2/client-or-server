(function(){
    function ClientOrServer(){

    }

    var base = ClientOrServer.prototype;

    base.global=function(){
        if(typeof window!=="undefined")
        {
            return window;
        }else{
            return global;
        }
    }

    base.isServer=function(){
        return typeof window==="undefined";
    }

    base.isClient=function(){
        return !(base.isServer());
    }

    function ServerClientCallback(){
        this._serverFunc=[];
        this._clientFunc=[];
    }

    var scc=ServerClientCallback.prototype;

    scc.server=function(func){
        this._serverFunc.push(func);
        return this;
    }

    scc.client=function(func){
        this._clientFunc.push(func);
        return this;
    }

    scc.run=function(cb){
        var runner=null;
        if(base.isClient()){
            runner =this._clientFunc;
        }else{
            runner = this._serverFunc;
        }

        for(var i =0;i<runner.length;i++){
            runner[i].apply(null);
        }

        this._clientFunc=[];
        this._serverFunc=[];

        if(cb!=null  && (typeof cb === "function")){
            cb.apply(null);
        }
        return this;
    }

    base.decide= base.d =function(){
        return new ServerClientCallback();
    }
    ClientOrServer.scc = ClientOrServer.serverClientCallback = ServerClientCallback;
    module.exports = exports = new ClientOrServer();
})();