var goods = module.exports = exports = {};
var cos = require("../");
goods["Server Client Test"] = function (test) {
    test.ok(cos.isServer());
    test.done();
};


goods["Test Enviroment Callback"] = function (test) {
    var one = 0;

    cos.decide().server(function () {
        one++;
        test.equal(typeof window, "undefined");
        test.equal(one, 1);
    }).client(
        function () {
            one--;
            test.notEqual(typeof window, "undefined");
            test.equals(one, -1);
        }
    ).run(function () {
            test.done();
        });
};